package com.company;

import com.sun.source.tree.WhileLoopTree;

import java.io.IOException;
import java.util.Scanner;

public class Console
{
    public static void Println(String string)
    {
        System.out.println(string);
    }

    public static void Print(String string)
    {
        System.out.print(string);
    }

    public static int InputInt(String message)
    {
        int x = 0;
        boolean inputResult = false;

        do
        {
            try
            {
                Scanner scanner = new Scanner(System.in);
                System.out.print(message);
                x = scanner.nextInt();
                inputResult=true;
            }
            catch (Exception e)
            {
                inputResult=false;
            }

        }while (inputResult==false);

        return x;
    }

    public static char InputChar(String message)
    {
        char x = 0;
        boolean inputResult = false;

        do
        {
            try
            {
                System.out.print(message);
                x = (char) System.in.read();
                inputResult=true;
            }
            catch (Exception e)
            {
                inputResult=false;
            }

        }while (inputResult==false);

        return x;
    }

    public int Read() throws IOException
    {
        return System.in.read();
    }
}
