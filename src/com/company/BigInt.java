package com.company;

public class BigInt
{
    private int[] digits;

    public BigInt(int countDigits)
    {
        digits = new int[countDigits];

        for (int i = 0; i < countDigits; i++)
        {
            digits[i]=0;
        }
    }

    public BigInt(int[] digits)
    {
        //this.digits = digits;
        this.digits = new int[digits.length];

        for (int i = 0; i < digits.length; i++)
        {
            this.digits[i]=digits[i];
        }
    }

    public String ToString()
    {
        String output = "";

        for (int i = 0; i < digits.length; i++)
        {
            output+=Integer.toString(digits[i]);
        }

        return output;
    }


    public BigInt Add(BigInt bigInt)
    {
        if(digits.length==bigInt.digits.length)
        {
            for (int i = 0; i < digits.length; i++)
            {
                this.digits[i]+=bigInt.digits[i];
            }
        }

        return this;
    }

}
