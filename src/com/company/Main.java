package com.company;

public class Main {

    public static void main(String[] args)
    {
        /*Car c1 = new Car("Bentley");
        Car c2 = new Car("BMW");

        System.out.println(Car.getCountAllCars());

        System.out.println(c1.getId());*/

        /*int a,b;
        char action;

        a = Console.InputInt("input a: ");
        action = Console.InputChar("input action: ");
        b = Console.InputInt("input b: ");

        switch (action)
        {
            case '+':
                Console.Println("Sum = "+(a+b));
                break;
            case '-':
                Console.Println("Sub = "+(a-b));
                break;
        }*/


        BigInt bi1 = new BigInt(new int[]{1,2,3});
        BigInt bi2 = new BigInt(new int[]{2,3,4});
        BigInt bi3 = new BigInt(new int[]{3,4,5});

        Console.Println(bi1.ToString());
        Console.Println(bi2.ToString());
        Console.Println(bi3.ToString());

        /*bi1.Add(bi2);
        bi1.Add(bi3);*/

        bi1.Add(bi2).Add(bi3);

        Console.Println(bi1.ToString());

    }
}
